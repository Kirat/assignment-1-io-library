section .data
newline: db 10
numbers: db '0123456789'

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rdi, rax
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ;сохраняем, т.к caller-saved
    call string_length
    pop rdi ;достаем сохраненное значение
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, newline
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push -1
    xor r9, r9
    .find_numbers:
        mov rdx, 0
        mov rax, rdi
        mov rsi, 10
        div rsi
        push rdx
        mov rdi, rax
        cmp rdi, 0
        je .print_number
        jmp .find_numbers
    .print_number:
        cmp qword[rsp], -1
        je .end
        mov r9, [rsp]
        mov rdi, [numbers + r9]
        call print_char
        add rsp, 8 ; rsp calle-saved, поэтому не заботимся о его сохранении
        jmp .print_number
    .end:
        add rsp, 8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rsi, rdi
    shr rsi, 63
    cmp rsi, 1
    jne .print_number
    push rdi ; caller-saved - сохраняю
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print_number:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        mov r8b, [rsi + rcx]
        cmp byte[rdi + rdx], r8b
        jne .not_equals
        cmp byte[rdi + rdx], 0
        je .equals
        inc rdx
        inc rcx
        jmp .loop
    .not_equals:
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall
    cmp rax, 0
    je .EOF
    mov rax, [rsp]
    add rsp, 8
    ret 
    .EOF:
        add rsp, 8
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    .loop_first:
        cmp rsi, 1
        jb .EOF
        push rdx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rdx

        cmp rax, 0 ;проверка на конец ввода
        je .end
        cmp rax, 0x20 ;проверка на пробельные смиволы
        je .loop_first
        cmp rax, 0x9
        je .loop_first
        cmp rax, 0xA
        je .loop_first

        mov byte[rdi + rdx], al
        inc rdx
    .loop_second:
        cmp rdx, rsi
        jae .EOF

        push rdx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rdx

        cmp rax, 0 ;проверка на конец ввода
        je .end
        cmp rax, 0x20 ;проверка на пробельные смиволы
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end

        mov byte[rdi + rdx], al
        inc rdx
        jmp .loop_second
    .end:
        mov byte[rdi + rdx], 0
        mov rax, rdi
        ret
    .EOF:
        mov rax, 0
        ret
 

; Принимает указатель на строку, пытается
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    .loop:
        mov sil, byte[rdi + rdx]
        
        cmp sil, '0' ; проверки на то, что ввод в пределах [0-9]
        jb .end
        cmp sil, '9'
        ja .end

        sub sil, 48
        mov rcx, rax
        sal rcx, 1
        sal rax, 3
        add rax, rcx
        add rax, rsi
        inc rdx
        jmp .loop
    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .signed
    .unsigned:
        call parse_uint
        ret
    .signed:
        inc rdi
        call parse_uint
        cmp rdx, 0
        jbe .end
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jae .error
        mov r8b, byte[rdi + rax]
        mov byte[rsi + rax], r8b
        cmp byte[rsi + rax], 0
        je .end
        inc rax
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        ret
